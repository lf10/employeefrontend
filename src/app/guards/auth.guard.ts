import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../service/authentication/authentication.service';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate {
	constructor(
		private _router: Router,
		private _authenticationService: AuthenticationService
	) {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		if (this._authenticationService.isAuth) {
			return true;
		}

		this._router.navigate(['/login'], { queryParams: { returnUrl: state.url, message: "login_required" } });
		return false;
	}
}
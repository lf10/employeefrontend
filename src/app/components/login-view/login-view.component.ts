import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../service/authentication/authentication.service';

@Component({
	selector: 'app-login-view',
	templateUrl: './login-view.component.html',
	styleUrls: ['./login-view.component.css']
})
export class LoginViewComponent implements OnInit {
	private _messages: MessageCollection = {
		"session_refresh_failed": {
			text: "Current session could not be refreshed.",
			bgcolor: "#ff0000",
			color: "#ffffff"
		},
		"login_required": {
			text: "You must be logged in, to view this page.",
			bgcolor: "#ff0000",
			color: "#ffffff"
		},
		"incorrect_username_or_password": {
			text: "Username or password is incorrect.",
			bgcolor: "#ff0000",
			color: "#ffffff"
		},
		"logout_successful": {
			text: "Successfully logged out.",
			bgcolor: "#00ff00",
			color: "#000000"
		}
	}

	public messageData: any;

	constructor(
		private _route: ActivatedRoute,
		private _authenticationService: AuthenticationService,
		private _router: Router
	) {
	}

	ngOnInit(): void {
		if (this._authenticationService.isAuth) {
			let redirectRoute = this._route.snapshot.queryParamMap.get("returnUrl") ?? "employeemanagement";

			this._router.navigate([redirectRoute]);
		}

		this._route.queryParams
			.subscribe(x => {
				let message = x["message"];

				this.messageData = this._messages[message];
			});
	}

	submit(form: NgForm) {
		this._authenticationService.login(form.value)
			.then(_ => {
				let redirectRoute = this._route.snapshot.queryParamMap.get("returnUrl") ?? "employeemanagement";

				this._router.navigate([redirectRoute]);
			})
			.catch(x => {
				this._router.navigate(["login"], { queryParams: { message: "incorrect_username_or_password" } });
			});
	}
}

interface MessageCollection {
	[index: string]: {
		text: string,
		bgcolor: string,
		color: string
	}
}
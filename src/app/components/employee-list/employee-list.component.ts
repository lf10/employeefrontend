import { Component, OnInit } from '@angular/core';
import {Employee} from "../../Employee";
import {EmployeeDataServiceService} from "../../service/employee-data-service.service";
import {QualificationDataService} from "../../service/qualification-data-service";

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  //filters: string[];
  activeFilter: string;

  constructor(private employeeDataService: EmployeeDataServiceService, private qualificationDataService: QualificationDataService) {
  }

  ngOnInit(): void {
    this.employeeDataService.requestAllEmployees();
    this.employeeDataService.resetDetailEmployee();
    this.qualificationDataService.requestAllQualifications();
  }

  get employees(): Employee[] {
    return this.employeeDataService.getEmployees();
  }

  get filters(): string[] {
    return this.qualificationDataService.getQualifications()
      .map(q => q.designation);
  }

  filterByQualification(qualification: string) {
    this.employeeDataService.requestEmployeesByQualification(qualification);
    this.employeeDataService.resetDetailEmployee();
  }

  showEmployeeDetails(employee: Employee) {
    this.employeeDataService.showEmployeeDetails(employee.id);
    this.qualificationDataService.requestEmployeeQualifications(employee.id)
  }

}

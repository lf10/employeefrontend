import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEmployeeModuleComponent } from './edit-employee-module.component';

describe('EditEmployeeModuleComponent', () => {
  let component: EditEmployeeModuleComponent;
  let fixture: ComponentFixture<EditEmployeeModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditEmployeeModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditEmployeeModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {Employee} from "../../Employee";
import {EmployeeDataServiceService} from "../../service/employee-data-service.service";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-employee-module',
  templateUrl: './edit-employee-module.component.html',
  styleUrls: ['./edit-employee-module.component.css']
})
export class EditEmployeeModuleComponent implements OnInit {

  editEmployee: Employee;

  constructor(private employeeDataService: EmployeeDataServiceService, private route: ActivatedRoute) {
    this.editEmployee = new Employee();
  }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const employeeId = Number(routeParams.get('employeeId'));
    this.setEditEmployeeByEmployeeId(employeeId);
  }

  edit(): void {
    this.employeeDataService.editEmployee(this.editEmployee);
  }

  async setEditEmployeeByEmployeeId(employeeId: number): Promise<void> {
    this.editEmployee = await this.employeeDataService.getEmployeeById(employeeId);
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEmployeeModuleComponent } from './new-employee-module.component';

describe('NewEmplooyeeComponentComponent', () => {
  let component: NewEmployeeModuleComponent;
  let fixture: ComponentFixture<NewEmployeeModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewEmployeeModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEmployeeModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

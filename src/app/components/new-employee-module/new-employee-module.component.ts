import {Component, OnInit} from '@angular/core';
import {Employee} from "../../Employee";
import {EmployeeDataServiceService} from "../../service/employee-data-service.service";

@Component({
  selector: 'app-new-employee-module',
  templateUrl: './new-employee-module.component.html',
  styleUrls: ['./new-employee-module.component.css']
})
export class NewEmployeeModuleComponent implements OnInit {

  newEmployee: Employee;

  constructor(private employeeDataService: EmployeeDataServiceService) {
    this.newEmployee = new Employee();
  }

  ngOnInit(): void {
  }

  save(): void {
    this.employeeDataService.saveEmployee(this.newEmployee);
    this.newEmployee = new Employee();
  }
}

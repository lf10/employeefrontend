import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-module',
  templateUrl: './employee-module.component.html',
  styleUrls: ['./employee-module.component.css']
})
export class EmployeeModuleComponent implements OnInit {

  filters: string[];
  activeFilter: string;

  constructor() {  }

  ngOnInit(): void {
  }

}

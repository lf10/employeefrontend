import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QualificationModuleComponent } from './qualification-module.component';

describe('QualificationModuleComponent', () => {
  let component: QualificationModuleComponent;
  let fixture: ComponentFixture<QualificationModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QualificationModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QualificationModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {QualificationDataService} from "../../service/qualification-data-service";
import {Qualification} from "../../model/Qualification";

@Component({
  selector: 'app-qualification-module',
  templateUrl: './qualification-module.component.html',
  styleUrls: ['./qualification-module.component.css']
})
export class QualificationModuleComponent implements OnInit {

  newQualification: Qualification;

  constructor(private qualificationDataService: QualificationDataService) {
    this.newQualification = new Qualification("");
  }

  ngOnInit(): void {
    this.qualificationDataService.requestAllQualifications();
  }

  get qualifications(): Qualification[] {
    return this.qualificationDataService.getQualifications();
  }

  add() {
    this.qualificationDataService.saveQualification(this.newQualification);
    this.newQualification = new Qualification("");
  }

  delete(designation: string) {
    this.qualificationDataService.deleteQualification(designation);
  }

}

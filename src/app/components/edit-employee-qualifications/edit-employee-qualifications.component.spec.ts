import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEmployeeQualificationsComponent } from './edit-employee-qualifications.component';

describe('EditEmployeeQualificationsComponent', () => {
  let component: EditEmployeeQualificationsComponent;
  let fixture: ComponentFixture<EditEmployeeQualificationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditEmployeeQualificationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditEmployeeQualificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {QualificationDataService} from "../../service/qualification-data-service";
import {EmployeeDataServiceService} from "../../service/employee-data-service.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-edit-employee-qualifications',
  templateUrl: './edit-employee-qualifications.component.html',
  styleUrls: ['./edit-employee-qualifications.component.css']
})
export class EditEmployeeQualificationsComponent implements OnInit {

  qualificationToAdd: string;
  editEmployeeId: number;

  constructor(private qualificationDataService: QualificationDataService, private employeeDataService: EmployeeDataServiceService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    this.editEmployeeId = Number(routeParams.get('employeeId'));
    this.qualificationDataService.requestEmployeeQualifications(this.editEmployeeId);
  }

  get qualifications() {
    return this.qualificationDataService.getQualifications()
      .map(q => q.designation);;
  }

  get employeeQualifications() {
    return this.qualificationDataService.getEmployeeQualifications();
  }

  addQualificationToEmployee() {
    this.qualificationDataService.addQualificationToEmployee(this.editEmployeeId, this.qualificationToAdd);
  }

  removeQualificationFromEmployee(qualificationDesignation: string) {
    this.qualificationDataService.removeQualificationFromEmployee(this.editEmployeeId, qualificationDesignation);
  }

}

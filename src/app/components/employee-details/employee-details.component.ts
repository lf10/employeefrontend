import { Component, OnInit } from '@angular/core';
import {EmployeeDataServiceService} from "../../service/employee-data-service.service";
import {Employee} from "../../Employee";
import {QualificationDataService} from "../../service/qualification-data-service";

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  constructor(private employeeDataService: EmployeeDataServiceService, private qualificationDataService: QualificationDataService) { }

  ngOnInit(): void {
  }

  get employee(): Employee {
    return this.employeeDataService.detailEmployee;
  }

  delete(employeeId: number) {
    this.employeeDataService.deleteEmployee(employeeId);
    this.employeeDataService.resetDetailEmployee();
  }

  get employeeQualifications() {
    return this.qualificationDataService.getEmployeeQualifications();
  }

}

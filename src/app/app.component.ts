import { Component } from '@angular/core';
import { AuthenticationService } from './service/authentication/authentication.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	public get userLoggedIn() {
		return this._authenticationService.isAuth;
	}

	constructor(
		private _authenticationService: AuthenticationService
	) {
	}

	logout() {
		this._authenticationService.logout("logout_successful");
	}
}
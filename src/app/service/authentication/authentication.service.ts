import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AccessToken } from 'src/app/model/AccessToken';

@Injectable({
	providedIn: 'root'
})
export class AuthenticationService {
	private readonly AUTH_URL = "http://authproxy.szut.dev/";

	private _failureCount = 0;
	private _accessToken: AccessToken | null = null;
	private _loginTime: number | null = null;

	public get isAuth() {
		if (this._accessToken && this._accessToken.expires_in && this._loginTime) {
			let timeDiff = Math.round((Date.now() - this._loginTime) / 1000);
			if (timeDiff < this._accessToken.expires_in) {
				return true;
			}

			this.refreshSession(this._accessToken.refresh_token!);

			return true;
		}

		return false;
	}

	public get accessToken() {
		return this._accessToken;
	}

	constructor(
		private router: Router,
		private http: HttpClient
	) {
		let sessionData = JSON.parse(localStorage.getItem("session") ?? "{}");
		let accessToken = sessionData["token_data"];
		let loginTime = sessionData["login_time"];

		if (accessToken && loginTime) {
			this._accessToken = accessToken;
			this._loginTime = loginTime;
		}
	}

	public login(user: { username: string, password: string }): Promise<{}> {
		let body = new URLSearchParams();
		body.set("grant_type", "password");
		body.set("client_id", "employee-management-service");
		body.set("username", user.username);
		body.set("password", user.password);

		let options = {
			headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		};

		return new Promise((res, rej) => {
			if (this._failureCount > 2) {
				rej({
					error: "801 User Blocked"
				});
				return;
			}
			this.http.post<AccessToken>(this.AUTH_URL, body.toString(), options)
				.subscribe({
					next: tokenData => {
						this._accessToken = tokenData;
						this._loginTime = Date.now();

						localStorage.setItem("session", JSON.stringify({
							login_time: this._loginTime,
							token_data: tokenData
						}));

						res({});
					},
					error: error => {
						this._failureCount++;
						rej(error);
					}
				});
		});
	}

	public logout(logoutMessage: string) {
		localStorage.removeItem("session");
		this._accessToken = null;
		this.router.navigate(['login'], { queryParams: { message: logoutMessage } });
	}

	private refreshSession(refreshToken: string) {
		let body = new URLSearchParams();
		body.set("grant_type", "refresh_token");
		body.set("refresh_token", refreshToken);

		let options = {
			headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		};

		return new Promise((res, rej) => {
			this.http.post<AccessToken>(this.AUTH_URL, body.toString(), options)
				.subscribe({
					next: tokenData => {
						this._accessToken = tokenData;

						localStorage.setItem("session", JSON.stringify({
							login_time: Date.now(),
							token_data: tokenData
						}));

						res({});
					},
					error: error => {
						this.logout("session_refresh_failed");
						rej(error);
					}
				});
		});
	}
}
import { TestBed } from '@angular/core/testing';

import { QualificationDataService } from './qualification-data-service';

describe('QualificationDataServiceService', () => {
  let service: QualificationDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QualificationDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

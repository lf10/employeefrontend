import {Injectable} from '@angular/core';
import {Qualification} from "../model/Qualification";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Employee} from "../Employee";
import {Observable} from "rxjs";
import { AuthenticationService } from './authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class QualificationDataService {

  qualifications: Qualification[] = [];
  employeeQualifications: Qualification[] = [];

  constructor(
    private http: HttpClient,
    private _authenticationService: AuthenticationService
  ) {
    this.requestAllQualifications();
  }

  requestAllQualifications() {
    this.http.get<Qualification[]>("/backend/qualifications", {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this._authenticationService.accessToken.access_token}`)
    }).subscribe(qualifications => this.qualifications = qualifications)
  }

  getQualifications(): Qualification[] {
    return this.qualifications;
  }

  async requestEmployeeQualifications(employeeId: number) {
    const data: any = await this.http.get<Qualification[]>('/backend/employees/' + employeeId + '/qualifications', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this._authenticationService.accessToken.access_token}`)
    }).toPromise();
    this.employeeQualifications = data.skillSet;
  }

  getEmployeeQualifications() {
    return this.employeeQualifications;
  }

  async saveQualification(qualification: Qualification) {
    //Post in die Datenbank
    await this.http.post<Qualification>("/backend/qualifications", qualification, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this._authenticationService.accessToken.access_token}`)
    }).pipe(
      //catchError(this.handleError('saveEmployee', employee))
    ).toPromise();
    this.requestAllQualifications()
  }

  async deleteQualification(designation: string): Promise<Observable<unknown>> {
    const data: any = await this.http.delete("/backend/qualifications", {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this._authenticationService.accessToken.access_token}`),
      body: {
        "designation": designation
      }
    }).toPromise();
    this.requestAllQualifications()
    return data;
  }

  async addQualificationToEmployee(employeeId: number, qualificationDesignation: string) {
    await this.http.post('/backend/employees/' + employeeId + '/qualifications', new Qualification(qualificationDesignation), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this._authenticationService.accessToken.access_token}`)
    }).pipe(
      //catchError
    ).toPromise();
    this.requestEmployeeQualifications(employeeId);
  }

  async removeQualificationFromEmployee(employeeId: number, qualificationDesignation: string): Promise<Observable<unknown>> {
    const data: any = await this.http.delete('backend/employees/' + employeeId + '/qualifications', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this._authenticationService.accessToken.access_token}`),
      body: {
        "designation": qualificationDesignation
      }
    }).toPromise();
    this.requestEmployeeQualifications(employeeId);
    return data;
  }
}

import {Injectable} from '@angular/core';
import {Employee} from "../Employee";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AuthenticationService} from './authentication/authentication.service';
import {Observable} from "rxjs";
import {Qualification} from "../model/Qualification";

@Injectable({
  providedIn: 'root'
})
export class EmployeeDataServiceService {
  employees: Employee[] = [];
  detailEmployee: Employee;

  constructor(
    private http: HttpClient,
    private _authenticationService: AuthenticationService
  ) {
    this.requestAllEmployees();
  }

  requestAllEmployees() {
    this.http.get<Employee[]>("/backend/employees", {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this._authenticationService.accessToken.access_token}`)
    }).subscribe(employees => this.employees = employees)
  }

  async requestEmployeesByQualification(qualification: string) {
    if (qualification) {
      const data: any = await this.http.get<Employee[]>('/backend/qualifications/' + qualification + '/employees', {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer ${this._authenticationService.accessToken.access_token}`)
      }).toPromise();
      this.employees = data.employees;
    } else {
      //Filter reset; show all employees
      this.requestAllEmployees();
    }
  }

  getEmployees(): Employee[] {
    return this.employees;
  }

  //Post in die Datenbank
  async saveEmployee(employee: Employee) {
    await this.http.post<Employee>("/backend/employees", employee, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this._authenticationService.accessToken.access_token}`)
    }).pipe(
      //catchError(this.handleError('saveEmployee', employee))
    ).toPromise();
  }

  //Post in die Datenbank
  async editEmployee(employee: Employee) {
    await this.http.put<Employee>("/backend/employees/" + employee.id, employee, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this._authenticationService.accessToken.access_token}`)
    }).pipe(
      //catchError(this.handleError('saveEmployee', employee))
    ).toPromise();
  }

  async deleteEmployee(id: number): Promise<Observable<unknown>> {
    const data: any = await this.http.delete('backend/employees/' + id, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this._authenticationService.accessToken.access_token}`)
    }).toPromise();
    this.requestAllEmployees();
    return data;
  }

  async showEmployeeDetails(id: number) {
    try {
      const data: any = await this.http.get('/backend/employees/' + id, {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer ${this._authenticationService.accessToken.access_token}`)
      }).toPromise();
      this.detailEmployee = data;
    } catch (e) {
      alert('Could not find the requested employee');
    }
  }

  resetDetailEmployee() {
    this.detailEmployee = null;
  }

  async getEmployeeById(id: number) {
    try {
      let employee: Employee = await this.http.get('/backend/employees/' + id, {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer ${this._authenticationService.accessToken.access_token}`)
      }).toPromise();
      return employee;
    } catch (e) {
      alert('Could not find the requested employee');
      return null;
    }
  }
}

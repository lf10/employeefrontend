import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TableModule } from 'primeng/table';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { NewEmployeeModuleComponent } from './components/new-employee-module/new-employee-module.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { EmployeeDetailsComponent } from './components/employee-details/employee-details.component';
import { CardModule } from "primeng/card";
import { ButtonModule } from 'primeng/button';
import { DividerModule } from 'primeng/divider';
import { EmployeeModuleComponent } from './components/employee-module/employee-module.component';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from "@angular/forms";
import { InputTextModule } from 'primeng/inputtext';
import { QualificationModuleComponent } from './components/qualification-module/qualification-module.component';
import { PanelModule } from "primeng/panel";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditEmployeeModuleComponent } from './components/edit-employee-module/edit-employee-module.component';
import { RouterModule } from '@angular/router';
import { LoginViewComponent } from './components/login-view/login-view.component';
import { AuthGuard } from './guards/auth.guard';
import { EditEmployeeQualificationsComponent } from './components/edit-employee-qualifications/edit-employee-qualifications.component';

@NgModule({
	declarations: [
		AppComponent,
		NewEmployeeModuleComponent,
		QualificationModuleComponent,
		EditEmployeeModuleComponent,
		EmployeeDetailsComponent,
		EmployeeModuleComponent,
		EmployeeListComponent,
		LoginViewComponent,
  EditEmployeeQualificationsComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		InputTextModule,
		FormsModule,
		ButtonModule,
		PanelModule,
		BrowserAnimationsModule,
		TableModule,
		CardModule,
		DividerModule,
		DropdownModule,
		RouterModule.forRoot([
			{ path: 'login', component: LoginViewComponent },
			{ path: '', component: EmployeeModuleComponent, canActivate: [AuthGuard] },
			{ path: 'employeemanagement', component: EmployeeModuleComponent, canActivate: [AuthGuard] },
			{ path: 'employeemanagement/new', component: NewEmployeeModuleComponent, canActivate: [AuthGuard] },
			{ path: 'employeemanagement/edit/:employeeId', component: EditEmployeeModuleComponent, canActivate: [AuthGuard] },
			{ path: 'qualificationmanagement', component: QualificationModuleComponent, canActivate: [AuthGuard] }
		])
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
